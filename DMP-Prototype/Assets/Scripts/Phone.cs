﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

[ExecuteInEditMode]
public class Phone : MonoBehaviour
{


    public GameObject Home, Charge, powerOn, Cam, Cast, Root;
    [SerializeField]
    Image ChargeImage;
    [SerializeField]
    Animator Animation;
    [SerializeField]
    ParticleSystem casetFX;
    [SerializeField]
    Transform Low, High, Aim;

    [Range(0, 1)]
    public float CurruntCharge = 1;
    public GameObject mesh;
    public bool ScreenOn = true;
    public CurruntScreen curruntScreen;
    public AnimPosition AnimationPoz;
    public TrackPositions PhoneTrackPoz;
    public AudioSource speaker;
    public bool OnPlayer;
    public bool Casting;

   void Awake()
    {
        Game.PlayerPhone = this;

    }

    void Start()
    {
        casetFX.Stop();

    }

    void Update()
    {
        ChargeImage.fillAmount = CurruntCharge;

        if (Casting == true && casetFX.isStopped)
        {
            casetFX.Play();
        }
        if (Casting == false && casetFX.isPlaying)
        {
            casetFX.Stop();
        }
        switch (curruntScreen)
        {
            case CurruntScreen.HOME:
                SetScreen(Home);
                break;
            case CurruntScreen.CHARGE:
                SetScreen(Charge);
                break;
            case CurruntScreen.POWERON:
                SetScreen(powerOn);
                break;
            case CurruntScreen.CAM:
                SetScreen(Cam);
                break;
            case CurruntScreen.CAST:
                SetScreen(Cast);
                break;
            case CurruntScreen.OFF:
                SetScreen(Root);

                break;
        }
    
        switch (AnimationPoz)
        {
            case AnimPosition.NORMAL:
                Animation.SetBool("Rotate", false);
                Animation.SetBool("ShowBack", false);
                Animation.SetBool("Point", false);
                Animation.SetBool("Cam", false);
                break;

            case AnimPosition.SPIN:
                Animation.SetBool("Rotate", true);
                Animation.SetBool("ShowBack", false);
                Animation.SetBool("Point", false);
                Animation.SetBool("Cam", false);
                break;

            case AnimPosition.BACK:
                Animation.SetBool("Rotate", false);
                Animation.SetBool("ShowBack", true);
                Animation.SetBool("Point", false);
                Animation.SetBool("Cam", false);
                break;

            case AnimPosition.POINT:
                Animation.SetBool("Rotate", false);
                Animation.SetBool("ShowBack", false);
                Animation.SetBool("Point", true);
                Animation.SetBool("Cam", false);
                break;

            case AnimPosition.CAM:
                Animation.SetBool("Rotate", false);
                Animation.SetBool("ShowBack", false);
                Animation.SetBool("Point", false);
                Animation.SetBool("Cam", true);
                break;
                

        }
    }

    void FixedUpdate()
{
    switch (PhoneTrackPoz)
    {
        case TrackPositions.CAMREAAIM:
            OnPlayer = true;
            transform.position = Vector3.Lerp(transform.position, Aim.transform.position, Time.fixedDeltaTime * 8);
            transform.rotation = Quaternion.Lerp(transform.rotation, Aim.transform.rotation, Time.fixedDeltaTime * 8);
            break;
        case TrackPositions.BODYTRACKHIGH:
            OnPlayer = true;
            transform.position = Vector3.Lerp(transform.position, High.transform.position, Time.fixedDeltaTime * 8);
            transform.rotation = Quaternion.Lerp(transform.rotation, High.transform.rotation, Time.fixedDeltaTime * 8);
            break;
        case TrackPositions.BODYTRACKLOW:
            OnPlayer = true;
            transform.position = Vector3.Lerp(transform.position, Low.transform.position, Time.fixedDeltaTime * 8);
            transform.rotation = Quaternion.Lerp(transform.rotation, Low.transform.rotation, Time.fixedDeltaTime * 8);
            break;

        case TrackPositions.None:
            break;
    }
}

public void GoToTrancform(Transform poz, float speed)
{
    OnPlayer = false;

    PhoneTrackPoz = TrackPositions.None;
    transform.position = Vector3.Lerp(transform.position, poz.transform.position, Time.fixedDeltaTime * speed);
    transform.rotation = Quaternion.Lerp(transform.rotation, poz.transform.rotation, Time.fixedDeltaTime * speed);
}
public void GoToAndScail(Transform poz, float speed)
{
    OnPlayer = false;

    PhoneTrackPoz = TrackPositions.None;
    transform.position = Vector3.Lerp(transform.position, poz.position, Time.fixedDeltaTime * speed);
    transform.rotation = Quaternion.Lerp(transform.rotation, poz.rotation, Time.fixedDeltaTime * speed);
    transform.localScale = Vector3.Lerp(transform.localScale, poz.lossyScale, Time.fixedDeltaTime * speed);

}


void SetScreen(GameObject screen)
{
    Home.SetActive(false);
    Charge.SetActive(false);
    powerOn.SetActive(false);
    Cam.SetActive(false);
    Cast.SetActive(false);

    screen.SetActive(true);

}
public enum CurruntScreen
{
    HOME,
    CHARGE,
    POWERON,
    CAST,
    CAM,
    OFF
}

public enum AnimPosition
{
    NORMAL,
    SPIN,
    BACK,
    CAM,
    POINT
}

public enum TrackPositions
{
    CAMREAAIM,
    BODYTRACKHIGH,
    BODYTRACKLOW,
    None

}


}
