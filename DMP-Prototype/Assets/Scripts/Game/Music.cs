﻿using UnityEngine;
using UnityEngine.Audio;
using System.Collections;

public class Music : MonoBehaviour {

    [SerializeField]
    AudioMixer Mixer;

    public AudioMixerSnapshot[] RoomSnapShots;
    public AudioSource[] Sources;


	void Start ()
    {
        Game.music = this;
	}
	
	void Update ()
    {

    }
}
