﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HudManager : MonoBehaviour
{

    [SerializeField]
    GameObject Help, pause, end, hud;
    bool MouseLock = true;

    [SerializeField]
    Image Reticule;
    public Memus menu;
    bool paused = false;

    void Awake()
    {
        Game.Hud = this;
        menu = Memus.GAME;
    }

    void Start()
    {
        menu = Memus.GAME;
    }

    void Update()
    {


        if (Input.GetKeyDown(KeyCode.Escape))
        {
            paused = !paused;
            if (paused)
            {
                menu = Memus.PAUSE;
            }
            else
            {
                MouseLock = true;
                menu = Memus.GAME;
            }
        }



        if (Game.RoomNumber == 5)
        {
            MouseLock = false;

            menu = Memus.END;
        }

        if (Input.GetKeyDown(KeyCode.Tab))
        {
            MouseLock = !MouseLock;
        }
        Screen.lockCursor = MouseLock;

        Reticule.enabled = Game.ShowRedicule;

        switch (menu)
        {
            case Memus.GAME:
                SetMenu(hud);
                Time.timeScale = 1;

                break;
            case Memus.CONTROLES:
                SetMenu(Help);
                Time.timeScale = 1;

                break;
            case Memus.PAUSE:
                MouseLock = false;
                SetMenu(pause);
                Time.timeScale = 0;
                break;
            case Memus.END:
                MouseLock = false;
                SetMenu(end);
                Time.timeScale = 1;

                break;
            default:
                break;
        }

    }
    void SetMenu(GameObject menuScreen)
    {
        Help.SetActive(false);
        pause.SetActive(false);
        end.SetActive(false);
        hud.SetActive(false);

        menuScreen.SetActive(true);

    }
    public void UnPause()
    {
        menu = Memus.GAME;
        MouseLock = true;
    }
    public void Restart()
    {
        Application.LoadLevel(Application.loadedLevel);
    }
    public void Exit()
    {
        Application.Quit();
    }
    public enum Memus
    {
        GAME,
        CONTROLES,
        PAUSE,
        END
    }
}
