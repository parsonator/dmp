﻿using UnityEngine;
using System.Collections;

public class Speaker : MonoBehaviour {

    public bool IsSpeakerOn;

    [SerializeField]
    ParticleSystem affect;

    public AudioSource Sound;

    void Update()
    {
        if(IsSpeakerOn)
        {
            if(affect.isStopped)
            {
                affect.Play();
            }
        }
        else
        {
            if (affect.isPlaying)
            {
                affect.Stop();
            }
        }
    }



}
