﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PickupPhone : MonoBehaviour
{

    public Camera phoneCamera;

    [SerializeField]
    InfoWindow phoneInfoWindow, ChargeInfo, ChamreaInfo;

    [SerializeField]
    SpeakerRoom spekerRoom;

    [Space(10)]

    public DoorOperator doorOne, doorTwo, doorThree, doorFour, lastDoor;
    [Space(10)]

    public Transform chargingPOS;
    Vector3 startPoz;
    public AudioSource soundMaster;

    [SerializeField]
    GameObject IntroFX, ChargingFX;

    public bool pickedUp;
    public bool charging;
    public bool cameraMode;
    public bool cameraOn;
    public bool Pointing;
    [Space(10)]

    public bool takingPic;

    public int PictuersTaken;

    public int oldMask;

    public AudioClip picSound;

    public LayerMask TriggerMask;
    public LayerMask PhoneMask;


    bool donecharging;

    bool prez1, prez2, prez3, prez4, prez5;

    // Use this for initialization

    void Awake()
    {
        Game.phonePickup = this;

    }
    void Start()
    {
        oldMask = phoneCamera.cullingMask;
        startPoz = Game.PlayerPhone.gameObject.transform.position;
        Game.PlayerPhone.gameObject.transform.position = Vector3.zero;
        Game.PlayerPhone.curruntScreen = Phone.CurruntScreen.OFF;
        StartCoroutine(BeginGame());
    }

    void Update()
    {

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Game.RoomNumber == 0)
        {
            Game.music.RoomSnapShots[4].TransitionTo(2);

            if (Physics.Raycast(ray, out hit, 3f, TriggerMask))
            {
                if (hit.collider.tag == "phone")
                {
                    if (Input.GetMouseButtonDown(0) && !prez1)
                    {
                        StartCoroutine(beginFirst());
                    }
                }
            }
        }
        if (Game.RoomNumber == 1)
        {

            if (!prez3 && !charging) Charingintro();

            Game.music.RoomSnapShots[2].TransitionTo(2);
            if (Physics.Raycast(ray, out hit, 3f, PhoneMask))
            {
                if (hit.collider.tag == "charger")
                {
                    if (charging == false && !donecharging)
                    {
                        if (Input.GetMouseButtonDown(0) && !prez2)
                        {
                            charging = true;
                            StartCoroutine(chargingProcess());
                        }
                    }
                    if (donecharging)
                    {
                        if (Input.GetMouseButtonDown(0))
                        {
                            Game.PlayerPhone.PhoneTrackPoz = Phone.TrackPositions.BODYTRACKLOW;
                        }
                    }

                }
            }
        }

        if (Game.RoomNumber == 2)
        {
            Game.music.RoomSnapShots[1].TransitionTo(2);
            if (Input.GetMouseButtonDown(1) && cameraMode)
            {
                cameraOn = !cameraOn;
            }
            cameraMode = true;
            if (!prez2)StartCoroutine(cameraProcess());
            if (PictuersTaken > 3)
            {
                doorThree.doorOpened = true;
            }
        }
        else
        {
            cameraMode = false;
        }

        if (Game.RoomNumber == 3)
        {


            Game.PlayerPhone.curruntScreen = Phone.CurruntScreen.CAST;
            Pointing = true;
            if (Input.GetMouseButtonDown(0))
            {
                Game.PlayerPhone.Casting = true;

                if (Physics.Raycast(ray, out hit, 10f, PhoneMask))
                {


                    if (hit.collider.tag == "MusicBox")
                    {
                        spekerRoom.casted = true;
                    }

                }

            }
            else
            {
                Game.PlayerPhone.Casting = false;
            }

        }
        else
        {
            Pointing = false;
            Game.PlayerPhone.Casting = false;
            spekerRoom.casted = false;
        }


    }

    void FixedUpdate()
    {
        if (Pointing)
        {
            Game.PlayerPhone.AnimationPoz = Phone.AnimPosition.POINT;
            Game.PlayerPhone.PhoneTrackPoz = Phone.TrackPositions.CAMREAAIM;
        }

        if (cameraMode == true)
        {
            Game.PlayerPhone.curruntScreen = Phone.CurruntScreen.CAM;
            Game.PlayerPhone.AnimationPoz = Phone.AnimPosition.CAM;
            Game.ShowRedicule = false;
            if (cameraOn == false)
            {
                Game.PlayerPhone.PhoneTrackPoz = Phone.TrackPositions.BODYTRACKLOW;
            }
            if (cameraOn == true)
            {
                Game.PlayerPhone.PhoneTrackPoz = Phone.TrackPositions.CAMREAAIM;

                if (takingPic == false && Input.GetMouseButtonDown(0))
                {
                    StartCoroutine(picTaken());
                    takingPic = true;
                }
            }
        }
        else
        {
            Game.ShowRedicule = true;
        }

        if (charging == true)
        {
            Game.PlayerPhone.curruntScreen = Phone.CurruntScreen.CHARGE;
            Game.PlayerPhone.CurruntCharge = Mathf.Lerp(Game.PlayerPhone.CurruntCharge, 1, Time.fixedDeltaTime * 0.3f);
            Game.PlayerPhone.GoToTrancform(chargingPOS, 10);
        }
    }



    IEnumerator beginFirst()
    {
        Game.PlayerPhone.PhoneTrackPoz = Phone.TrackPositions.BODYTRACKHIGH;
        Game.PlayerPhone.AnimationPoz = Phone.AnimPosition.NORMAL;
        yield return new WaitForSeconds(3);
        phoneInfoWindow.SetInfoText("Introducing the Blade");
        yield return new WaitForSeconds(3);
        Game.PlayerPhone.curruntScreen = Phone.CurruntScreen.HOME;
        phoneInfoWindow.SetInfoText("This is the phone thats going to change everything");
        yield return new WaitForSeconds(3);
        phoneInfoWindow.SetInfoText("The cutting edge of mobile technology");
        Game.PlayerPhone.AnimationPoz = Phone.AnimPosition.SPIN;
        yield return new WaitForSeconds(4);
        Game.PlayerPhone.AnimationPoz = Phone.AnimPosition.NORMAL;
        phoneInfoWindow.hidePannle();
        Game.PlayerPhone.PhoneTrackPoz = Phone.TrackPositions.BODYTRACKLOW;
        doorOne.doorOpened = true;
        prez1 = true;
    }
    
    void Charingintro()
    {
        ChargeInfo.SetInfoText("Place your phone on the pad");
        prez3 = true;
    }

    IEnumerator chargingProcess()
    {
        ChargingFX.SetActive(true);
        ChargeInfo.ShowPannle();
        Game.PlayerPhone.AnimationPoz = Phone.AnimPosition.NORMAL;
        yield return new WaitForSeconds(3);
        ChargeInfo.SetInfoText("No longer wait for a phone to charge.");
        yield return new WaitForSeconds(3);
        ChargeInfo.SetInfoText("With the new Plasma wireless charging your phone will wait for you.");
        yield return new WaitForSeconds(3);
        ChargeInfo.SetInfoText("Under half an hour charge");
        yield return new WaitForSeconds(3);
        charging = false;
        ChargeInfo.hidePannle();
        donecharging = true;
        doorTwo.doorOpened = true;


    }

    IEnumerator BeginGame()
    {
        Game.Hud.menu = HudManager.Memus.CONTROLES;
        yield return new WaitForSeconds(4);
        Game.Hud.menu = HudManager.Memus.GAME;
        yield return new WaitForSeconds(0.1f);

        Game.PlayerPhone.curruntScreen = Phone.CurruntScreen.POWERON;

        yield return new WaitForSeconds(6);
        Game.PlayerPhone.gameObject.transform.position = startPoz;

    }




    public IEnumerator cameraProcess()
    {
        prez2 = true;
        pickedUp = false;
        ChamreaInfo.ShowPannle();
        ChamreaInfo.SetInfoText("Try out the 20Mpx rear camrea.");
        yield return new WaitForSeconds(5);
        ChamreaInfo.SetInfoText("Press right click to aim. left click to take a picture");
        yield return new WaitForSeconds(5);
        ChamreaInfo.SetInfoText("Take 4 or 5 pictures.");
        yield return new WaitForSeconds(5);
        ChamreaInfo.ShowPannle();
    }

    IEnumerator picTaken()
    {
        phoneCamera.clearFlags = CameraClearFlags.Nothing;
        phoneCamera.cullingMask = 0;
        soundMaster.PlayOneShot(picSound);
        yield return new WaitForSeconds(1);
        phoneCamera.clearFlags = CameraClearFlags.Skybox;
        phoneCamera.cullingMask = oldMask;
        PictuersTaken++;
        takingPic = false;
    }


}

