﻿using UnityEngine;
using System.Collections;

public class DoorTrigger : MonoBehaviour {

    [SerializeField] DoorOperator Door;
    [SerializeField] bool KeepOpen;

    bool plusRoom;

	void OnTriggerEnter (Collider other)
    {
	    if (other.gameObject.tag == "Player")

        {
            Door.doorOpened = KeepOpen;
            gameObject.SetActive(false);
            if(!plusRoom)
            {
                Game.RoomNumber++;
                plusRoom = true;
            }

        }
	}

}
