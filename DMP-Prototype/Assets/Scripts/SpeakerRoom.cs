﻿using UnityEngine;
using System.Collections;

public class SpeakerRoom : MonoBehaviour
{
    public InfoWindow MusicInfo;
    [SerializeField]
    Speaker[] RoomSpeakers;
    [SerializeField]
    AudioClip Song;
    public bool casted;

    bool MusicPlaying = true;
    bool PlayingOnphone;

    bool IntroDone;
    bool SequenceStarted;
    // Use this for initialization
    void Start()
    {
        setSong();
        StopMusic();
    }

    // Update is called once per frame
    void Update()
    {
        if (Game.RoomNumber == 3)
        {
            if (!IntroDone)
                StartCoroutine(MusicIntro());

            Game.music.Sources[4].clip = null;
            Game.music.RoomSnapShots[5].TransitionTo(1);
            if (casted)
            {
                PlayingOnphone = false;
                Game.music.RoomSnapShots[4].TransitionTo(2);
                if (!SequenceStarted)
                {
                    StopCoroutine(MusicIntro());
                    StartCoroutine(MusicSequence());
                }
            }
            else
            {
                PlayingOnphone = true;
                Game.music.RoomSnapShots[5].TransitionTo(2);

            }
            if (PlayingOnphone)
            {
                Game.music.RoomSnapShots[5].TransitionTo(1);
                StopMusic();
            }
        }
        else
        {
            StopMusic();
        }
    }


    void PlayMusic()
    {
        if (!MusicPlaying)
        {
            foreach (Speaker speaker in RoomSpeakers)
            {
                speaker.Sound.Play();
                speaker.IsSpeakerOn = true;
            }
            MusicPlaying = true;

        }
    }
    void StopMusic()
    {
        if (MusicPlaying)
        {
            foreach (Speaker speaker in RoomSpeakers)
            {
                speaker.Sound.Stop();
                speaker.IsSpeakerOn = false;

            }
            MusicPlaying = false;

        }
    }

    void setSong()
    {
        foreach (Speaker speaker in RoomSpeakers)
        {
            speaker.Sound.clip = Song;
        }
    }

    IEnumerator MusicIntro()
    {
        IntroDone = true;
        MusicInfo.ShowPannle();
        yield return new WaitForSeconds(6);
        MusicInfo.SetInfoText("The Blade alows you to cast audio to any device you point it at");
        yield return new WaitForSeconds(6);
        MusicInfo.SetInfoText("Click on the Mixer To Cast");
        yield return new WaitForSeconds(5);
        MusicInfo.hidePannle();
        yield return new WaitForSeconds(1);

    }


    IEnumerator MusicSequence()
    {
        SequenceStarted = true;

        Game.music.RoomSnapShots[4].TransitionTo(1);

        MusicInfo.ShowPannle();
        PlayMusic();
        yield return new WaitForSeconds(5);
        MusicInfo.SetInfoText("Enjoy the Music!");
        yield return new WaitForSeconds(9);
        Game.phonePickup.doorFour.doorOpened = true;
        MusicInfo.hidePannle();
        yield return new WaitForSeconds(1);
        Debug.Log("BOOT");

    }
}
