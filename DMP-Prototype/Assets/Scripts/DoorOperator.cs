﻿using UnityEngine;
using System.Collections;

public class DoorOperator : MonoBehaviour {

    public GameObject door;
    public Vector3 doorClose;
    public Transform doorOpen;

    [SerializeField]
    OcclusionPortal portal;
    public bool doorOpened;

	// Use this for initialization
	void Start () {
        doorClose = door.transform.position;

	}
	
	// Update is called once per frame
	void Update () 
    
    {
	    if (doorOpened == false)
        {
            door.transform.position = Vector3.Lerp(door.transform.position,doorClose, Time.deltaTime * 2.5f);
            portal.open = false;

        }

        if (doorOpened == true)
        {
            portal.open = true;
            door.transform.position = Vector3.Lerp(door.transform.position, doorOpen.position, Time.deltaTime * 2.5f);
        }
        // debug
        if(Input.GetKeyDown(KeyCode.F10))
        {
            doorOpened = true;
        }
        if (Input.GetKeyDown(KeyCode.F11))
        {
            doorOpened = false;
        }

    }
}
