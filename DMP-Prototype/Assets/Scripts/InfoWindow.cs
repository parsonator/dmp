﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class InfoWindow : MonoBehaviour {

    public Text TextField;
    public Transform Player;
	// Use this for initialization
	void Start ()
    {
	}
	
	// Update is called once per frame
	void Update () {
        /*
        switch (Hover)
        {
            case HoverLocation.UP:
                transform.InverseTransformPoint(transform.parent.up * 0.5f);
                break;
            case HoverLocation.RIGHT:
                transform.InverseTransformPoint(transform.parent.right * 0.5f);

                break;
            case HoverLocation.LEFT:
                transform.InverseTransformPoint(-transform.parent.right * 0.5f);

                break;
            default:
                break;
        }
        */
        transform.LookAt(Player);
    }

    public void SetInfoText(string text)
    {
        TextField.text = text;
    }
    public void Crossfade()
    {
        TextField.CrossFadeAlpha(0.0f, 0.2f, false);
        TextField.CrossFadeAlpha(1.0f, 0.5f, false);
    }
    public void hidePannle()
    {
        TextField.enabled = false;
    }
    public void ShowPannle()
    {
        TextField.enabled = true;
    }

}
